---
layout: post
title:  "IOKit - Power Management"
date:   2015-04-26 14:43:00
categories: Apple Mac
tags: mac os-x apple power-managment 
author:	Mitch Winn
---
Power management sometimes is a key functionality for apps that needs to be able to control the state of the machine when the app is used. IOKit provides some components that allow you to do just that. 

Recently, I used this library to prevent the Mac from sleeping in a application. The power manangement functionality in IOKit is designed to be able to be battery efficient and is mainly used by device drivers, but you can also implement this in any type of application to control sleep states of the computer.

To add the ability to prevent your Mac from sleeping, all you need to do is send the kernal an assertion with the type of assertion, assertion level, reason for the prevention, and the address to the variable that will hold the assertion id when it is created.

{% highlight Swift %}
/// Assertion type - prevent idle sleep.
let kIOPMAssertPreventUserIdleDisplaySleep = "PreventUserIdleDisplaySleep" as CFString

//...

// Create the assertion that will prevent the application from sleeping.
var success = IOPMAssertionCreateWithName(kIOPMAssertPreventUserIdleDisplaySleep,
                                          IOPMAssertionLevel(kIOPMAssertionLevelOn),
                                          reasonForActivity,
                                          &assertionID)
{% endhighlight %}

The method will either return `kIOReturnSuccess` or nil specifiying that the creation of the assertion failed.

When you want to quit the assertion activity, you need to just release the assertion.

{% highlight Swift %}
// If the assertion succeeded, relase it.
if success == kIOReturnSuccess {
    IOPMAssertionRelease(assertionID);
}
{% endhighlight %}

You can learn more about IOKit and power managment, which is only a tiny peice of IOKit, [here][ioKit-documentation].

[ioKit-documentation]:   https://developer.apple.com/library/mac/documentation/DeviceDrivers/Conceptual/IOKitFundamentals/PowerMgmt/PowerMgmt.html
